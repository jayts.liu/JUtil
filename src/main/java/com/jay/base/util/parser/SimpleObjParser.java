package com.jay.base.util.parser;


import java.lang.reflect.Field;

/**
 * 
 * @author Jay Liu
 * @date 20161018 ; update 20171214
 * @version 0.2
 */
public class SimpleObjParser {

	public static <E> E put(E obj, String fieldName, Object value) throws Exception {
		Field targetField = null;
		try {
			targetField = obj.getClass().getDeclaredField(fieldName);
			targetField.setAccessible(true);
			targetField.set(obj, value);
			
		} catch (Exception e) {
			throw e;
		} finally {
			targetField.setAccessible(false);
		}
		return obj;
	}
	
	public static <E> Object get(E obj, Field filed) throws Exception {
		Field targetField = null;
		try {
			targetField = obj.getClass().getDeclaredField(filed.getName());
			targetField.setAccessible(true);
			return targetField.get(obj);
		} catch (Exception e) {
			throw e;
		} finally {
			targetField.setAccessible(false);
		}
	}
	
	public static <E> Object get(E obj, String filed) throws Exception {
		Field targetField = null;
		try {
			targetField = obj.getClass().getDeclaredField(filed);
			targetField.setAccessible(true);
			return targetField.get(obj);
		} catch (Exception e) {
			throw e;
		} finally {
			targetField.setAccessible(false);
		}
	}
	
}

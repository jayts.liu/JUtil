package com.jay.base.util.common;

import java.util.Arrays;

/**
 * 
 * @author Jay.Liu
 * @update 20171129
 * @version 0.1
 */
public class StrSpliter {
	
	/**
	 * to separate string by upper case
	 * @param stringToSeparate 
	 * @return String[] result after split
	 */
	public static String[] splitByUpperCase(String stringToSeparate) {
		if(stringToSeparate==null || stringToSeparate.trim().length() == 0)
			return null;
		
		char[] clst = stringToSeparate.toCharArray();
		String[] resultList = new String[1];;
		StringBuilder node = new StringBuilder();
		
		int idx=0;
		for(int i=0; i < clst.length; i++){
			if(i!=0 && Character.isUpperCase(clst[i])){
				resultList = Arrays.copyOf(resultList, resultList.length+1);
				node.setLength(0);
				++idx;
			}

			node.append(clst[i]);
			resultList[idx] = node.toString();
		}
		return resultList;
	}
	


}

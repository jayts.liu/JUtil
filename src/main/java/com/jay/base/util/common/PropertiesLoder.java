package com.jay.base.util.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * 
 * @author Jay LIu
 * @version 0.2
 * @since 2017
 * @update 20180621
 */
public class PropertiesLoder {
	
	/**
	 * To load property file with user define values 
	 * @param filePath string of file path
	 * @return java.util.Properties
	 */
	public static Properties load(String filePath) {
		
		Properties properties = new Properties();

		try {
			boolean flag =false;
			try { //check from resource
				properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(filePath)); //*project root
				flag = true;
			} catch(Exception e) {}
			if(flag != true) 
				properties.load(new FileInputStream(filePath)); // load a properties file
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		return properties; 
	}
	
	/**
	 * To get key values
	 * @param filePath string of file path
	 * @param key string of property key
	 * @return string of result value 
	 */
	public static String getText(String filePath, String key) {
		String locRst = null;
		
		Properties properties = load(filePath); // load a properties file
		locRst = properties.getProperty(key);
		
		return locRst; 
	}
	
	/**
	 * Load property file by Class 
	 * @param clazz - class properties to load
	 * @return
	 */
	public static Properties load(Class<?> clazz) {
		return load(clazz.getSimpleName() + ".properties");
	}
	
	/**
	 * get key values form property file by Class 
	 * @param clazz - class properties to load
	 * @param key - String key to find
	 * @return
	 */
	public static String getText(Class<?> clazz, String key) {
		return getText(clazz.getSimpleName() + ".properties", key); 
	}
}

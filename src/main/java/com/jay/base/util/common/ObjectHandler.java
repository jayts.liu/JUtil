/* Copyright (C) 2018 JayLiu to Present. - All Rights Reserved. */
package com.jay.base.util.common;

import java.lang.reflect.Field;
import java.util.List;

public class ObjectHandler {
	
	public static String toString(Object object) {
		StringBuilder sb = new StringBuilder();
		Class<?> thisClass = null;
		try {
			thisClass = Class.forName(object.getClass().getName());

			Field[] fieldLst = thisClass.getDeclaredFields();
			sb.append(object.getClass().getSimpleName() + " [ ");
			for(int i=0; i < fieldLst.length; i++) {
				Field f = fieldLst[i];
				f.setAccessible(true);
				String fName = f.getName();
				
				if(i > 0) 
					sb.append(", \t ");
				sb.append("(" + f.getType() + ") " + fName + "=" + f.get(object));
			}
			sb.append("]");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public static void printObj(Object obj) {
		System.out.printf("*****Result: %s ***** \n", toString(obj));
	}
	
	public static void printList(List<?> objList) {
		System.out.println("objList size:" + objList.size());
		for(Object o: objList)
			printObj(o);
	}
}

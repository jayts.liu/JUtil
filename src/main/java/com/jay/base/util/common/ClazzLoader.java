package com.jay.base.util.common;

/**
 * 
 * @author Jay.Liu
 * @date 20171218
 * @version 0.1
 */
public final class ClazzLoader {
	
	public static Class<?> getClass(String className) {
		Class<?> c;
		try {
			c = Class.forName(className);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
		return c;
	}

}

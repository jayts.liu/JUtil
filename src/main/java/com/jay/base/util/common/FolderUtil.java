package com.jay.base.util.common;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Jay Liu(JayTs.Liu@gmail.com)
 * @date 20140328 update 20171222
 * @version 0.2
 */
public class FolderUtil {
	
	/**
	 * to get absolute path list by user specified path
	 * @param path - string path to scan
	 * @return String List with absolute path 
	 */
	public static List<String> getDirectoryList(String path){
		List<String> strList = new ArrayList<String>();

		String directory = null;
		File folder = new File(path);
		File[] directoryList = folder.listFiles();

		if(directoryList != null) {
			for (int i=0; i < directoryList.length; i++) {
				if (directoryList[i].isDirectory()) {
					try {
						directory = directoryList[i].getCanonicalPath();
						strList.addAll(getDirectoryList(directory));
						//System.out.println(directoryList[i].getCanonicalPath());
					} catch (IOException e) {
						directory = directoryList[i].getName();
						e.printStackTrace();
					}
					strList.add(directory);
				}
			}
		}
		return strList;
	}
	
	/**
	 * to get file list with absolute path by specified folder
	 * @param path - string path to scan
	 * @return String List with absolute path and file name
	 */
	public static List<String> getDirectoryFileList(String path, String... extensionFilenames) {
		return new ArrayList<String>(getDirectoryFileMap(path, extensionFilenames).keySet());
	}
	
	public static Map<String, String> getDirectoryFileMap(String path, String... extensionFilenames) {
		Map<String, String> map = new HashMap<>();

		String files;
		File folder = new File(path);
		File[] fileList = folder.listFiles();

		if(fileList != null) {
			for (int i = 0; i < fileList.length; i++) {
				if (fileList[i].isFile()) {
					try {
						files = fileList[i].getCanonicalFile().toString();
					} catch (IOException e) {
						files = fileList[i].getName();
						e.printStackTrace();
					}
					if(extensionFilenames == null || extensionFilenames.length == 0 || isMatch(files, extensionFilenames))
						map.put(files, files);
				}
			}
		}
		return map;
	}
	
	
	/**
	 * get file list include sub-folder by specified path
	 * @param path - string path to scan
	 * @param extensionFilenames specified file type to scan
	 * @return String List with absolute path and file name
	 */
	public static List<String> getTreeFiles(String path, String... extensionFilenames) {
		List<String> resultList = null;
		if(path != null && extensionFilenames != null) {
			resultList = new ArrayList<String>();
			List<String> dirLst = new ArrayList<String>();
			dirLst.add(path);
			dirLst.addAll(getDirectoryList(path));
			for(String locPath: dirLst) {
				List<String> resutl = getDirectoryFileList(locPath, extensionFilenames);
				if(resutl!=null)
					resultList.addAll(resutl);
			}
		}
		return resultList;
	}
	
	/**
	 * to get file list in specified folder
	 * @param path - string path to scan
	 * @return String List with file name only
	 */
	public static List<String> getFileList(String path) {
		List<String> strList = new ArrayList<String>();

		String files;
		File folder = new File(path);
		File[] fileList = folder.listFiles();

		for (int i = 0; i < fileList.length; i++) {

			if (fileList[i].isFile()) {
				files = fileList[i].getName();

				strList.add(files);
			}
		}

		return strList;
	}
	
	private static boolean isMatch(String extensionFilename, String... compares) {
		if(extensionFilename!=null && compares != null)
			for(String s : compares)
				if(extensionFilename.endsWith(s))
					return true;
		return false;
	}
	


}

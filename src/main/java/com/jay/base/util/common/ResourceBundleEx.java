package com.jay.base.util.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * 
 * @author Jay (JayTs.Liu@gmail.com)
 * @date 20180111
 * @version 0.1
 */
public class ResourceBundleEx {
	
	private final static String CONF_PATH = "res/language.properties";
	private final static Properties prop = getProperties();
	private final static String ROOT = prop.getProperty("language.root.path");

	public ResourceBundleEx() {

		Locale.setDefault(Locale.US);

		String language = prop.getProperty("local.language").trim();
		String country = prop.getProperty("local.country").trim();
		
		if(language != null && !language.equals("") && country != null && !country.equals(""))
			Locale.setDefault(new Locale(language, country));
		
		System.out.println("Locale:" + Locale.getDefault());
	}
	
	public String getString(String text) {
		System.out.println("Locale:" + Locale.getDefault());
		String className = Thread.currentThread().getStackTrace()[2].getClassName(); //get caller class name
		ResourceBundle locResourceBundle = ResourceBundle.getBundle(ROOT+className);
		return locResourceBundle.getString(text);
	}
	
	private static Properties getProperties() {
		Properties prop =  new Properties();
		try {
			prop.load(new FileInputStream(CONF_PATH));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return prop;
	}

}

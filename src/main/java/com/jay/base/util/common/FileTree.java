package com.jay.base.util.common;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author JayLiu (JayTs.Liu@gmail.com)
 * @Since 20180803
 * @version 0.1
 */
public class FileTree {
	
	private List<String> directoryList;
	private List<String> fileList;
	
	FileTree() {
		File currentDir = new File("."); // current directory
		doScan(currentDir);
		
		for(String s:fileList)
			System.out.println(s);
	}

	public static void main(String[] args) {
		new FileTree();
	}

	public void doScan(File dir) {
		
		try {
			File[] files = dir.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					//System.out.println("directory:" + file.getCanonicalPath());
					getDirectoryList().add(file.getCanonicalPath());
					doScan(file);
				} else {
					//System.out.println("     file:" + file.getCanonicalPath());
					getFileList().add(file.getCanonicalPath());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<String> getDirectoryList() {
		if(directoryList == null)
			directoryList = new ArrayList<>();
		return directoryList;
	}

	public void setDirectoryList(List<String> directoryList) {
		this.directoryList = directoryList;
	}

	public List<String> getFileList() {
		if(fileList == null)
			fileList = new ArrayList<>();
		return fileList;
	}

	public void setFileList(List<String> fileList) {
		this.fileList = fileList;
	}
	
	
}

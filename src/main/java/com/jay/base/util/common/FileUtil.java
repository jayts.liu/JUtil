/* Copyright (C) 2018 Jay to Present. - All Rights Reserved. */
package com.jay.base.util.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Jay (JayTs.Liu@gmail.com)
 * @date 20171227
 * @version 0.1
 */
public class FileUtil {
	
	public static boolean moveFile(String sourceFile, String targetFile) throws IOException {
		Path sPath = FileSystems.getDefault().getPath(sourceFile);
		Path tPath = FileSystems.getDefault().getPath(targetFile);
		
		if(Files.notExists(tPath.getParent()))
			Files.createDirectories(tPath.getParent());
		return (Files.move(sPath, tPath, StandardCopyOption.REPLACE_EXISTING)!=null ? true : false);
	}
	
	public static boolean copyFile(String sourceFile, String targetFile) throws IOException {
		return copyFile(sourceFile, targetFile, false);
	}
	
	public static boolean copyFile(String sourceFile, String targetFile, boolean isDeleteSourceFile) throws IOException {
		boolean successFlag = false;
		Path sPath = FileSystems.getDefault().getPath(sourceFile);
		Path tPath = FileSystems.getDefault().getPath(targetFile);
		
		if(Files.notExists(tPath.getParent()))
			Files.createDirectories(tPath.getParent());
		
		successFlag = Files.copy(sPath, tPath, StandardCopyOption.REPLACE_EXISTING)!=null ? true : false ;
		
		if(successFlag && isDeleteSourceFile) 
			successFlag = deleteFile(sourceFile);
		
		return successFlag;
	}
	
	public static boolean deleteFile(String targetFile) {
		File file = new File(targetFile);
		if(file.exists())
			return file.delete();
		return false;
	}
	
	public static void empty(File dir) throws FileNotFoundException {
		if (dir.exists() && dir.isDirectory()) {
			File[] entries = dir.listFiles();
			int sz = entries.length;
			for (int i = 0; i < sz; i++) {
				if (entries[i].isDirectory()) {
					deltree(entries[i]);
				} else {
					entries[i].delete();
				}
			}
		}
	}

	public static void deltree(File dir) throws FileNotFoundException {
		if (dir.exists() && dir.isDirectory()) {
			empty(dir);
			dir.delete();
		}
	}

	public static void deleteLocalFile(String dir) throws IOException {
		File myDir = new File(dir);
		if (myDir.exists() && myDir.isDirectory()) {
			empty(myDir);
			myDir.delete();
		}
	}
	
	/**
	 * To check file type by extension file name
	 * @param fileName - String file name to check
	 * @param extTypes - String[] Pattern of extension names to match
	 * @return boolean match result
	 */
	public static boolean matchExtName(String fileName, String... extTypes) {
		StringBuilder sb = new StringBuilder(); //([a-zA-Z0-9\s_\\.\-\(\):])+(.doc|.docx|.pdf)$
		for(String s: extTypes) {
			if(sb.length() > 0)
				sb.append("|");
			sb.append(s);
		}
		
		String regx = "(.+?)+("+sb.toString()+")$";

		Pattern p = Pattern.compile(regx , Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(fileName);
		if (m.find()) 
			return true;
		return false;
	}
}
